#Snap ArcherSay 0.1.0

Snap ArcherSay is a fun and NSFW CLI program with sassy quotes from TV's Archer in the style of cowsay.

##WARNING: THIS CONTAINS GRAPHIC LANGUAGE AND IS NOT SAFE FOR WORK OR YOUNG CHILDREN!

##Installation
This library requires PHP 5.4 or later; we recommend using the latest available version of PHP as a matter of principle.
It is installable and autoloadable via Composer as snap/archer-say.
```bash
composer require snap/archersay
```
For best results, install the project into a directory that is in your include path, for example in Linux or Mac, this can be ~/bin

The called file, archersay.php needs to be moved to the same level as the project directory, and on Linux/Mac it will need to be made executable can optionally be renamed to archersay
Windows users will need to retain the .php extension.
```bash
cd vendor/snap/archersay/src
chmod +x archersay.php
mv archersay.php ../../../../archersay
```

##Use Examples
On Linux/Mac, assuming you followed the instructions above and the archersay file is in a directory in your include path, you can call the program from anywhere.
```bash
# random quote
archersay
# tell Archer what to say
archersay "My name is Archer and I think you are awesome!"
# pipe output from another program into Archer's mouth (you must install fortune for this example)
fortune | archersay
```
On Windows, you will need to either be in the same directory as the archersay.php file or provide an absolute path to it.
```bash
# random quote
php archersay.php
# tell Archer what to say
php archersay.php "My name is Archer and I think you are awesome!"
```

##Troubleshooting
Permission denied - make sure you made the archersay/archersay.php file executable
archersay: command not found - the archersay file is not in your include path.  Add its parent directory to your include path, call it from within its parent directory or use an absolute path to it.
PHP not found - make sure PHP is installed properly by running php -v  If it isn't install it using your package manager.

Other questions or problems: Feel free to contact me through AlexFraundorf.com and I'll do my best to help :-)

Happy PHPing!
