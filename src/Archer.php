<?php
/*
 * Class to provide functionality for a fun cowsay style command line program 
 *  of Archer's famous quotes.
 * 
 * WARNING: THIS CONTAINS GRAPHIC LANGUAGE AND IS NOT SAFE FOR WORK OR YOUNG CHILDREN!
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2019, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\Say
 * @version 0.1.0 02/21/2019
 * @since 0.1.0 02/11/2019
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArcherSay;

class Archer extends \Snap\Say\SayAbstract {
    
    
    /**
     *
     * @var string title of the CLI application
     */
    protected $title = 'Archer Say (NSFW)';
    
    /**
     *
     * @var string description and simple useage instructions for the CLI application
     */
    protected $description = 'Have Archer say anything you tell him to by entering '
            . 'your text after the command ie: archersay "hi there" or simply '
            . 'run archersay for a random archerism from the text file. '
            . 'Piping output from another program is also supported, ie: '
            . '"fortune | archersay"';
    
    /**
     *
     * @var string version 
     */
    protected $version = '0.0.1 (02/21/2019)';
    
    
    /**
     * "Says" the quote by printing it to the CLI.
     * 
     * @param string $quote
     * @version 0.1.0 02/12/2019
     * @since 0.1.0 02/11/2019
     */
    protected function say($quote) {
        $max_length = $this->line_character_limit;
        $words = explode(' ', $quote);
        $lines = $this->getLines($words);
        $output_lines = '';
        foreach($lines as $line) {
            $line_out = '|     ' . $line;
            $spaces = 41 - strlen($line);
            for($i=1; $i<=$spaces; $i++) {
                $line_out .= ' ';
            }
            $line_out .= ' |' . PHP_EOL;
            if(strlen($line_out) > $max_length) {
                $max_length = strlen($line_out) - 6;
            }
            $output_lines .= $line_out;
        }
        $output = '  -';
        for($i=1; $i<=$max_length; $i++) {
            $output .= '-';
        }
        $output .= '-' . PHP_EOL . ' /';
        for($i=1; $i<=$max_length; $i++) {
            $output .= ' ';
        }
        $output .= '  \\' . PHP_EOL
                . $output_lines
                . ' \\';
        for($i=1; $i<=$max_length; $i++) {
            $output .= ' ';
        }
        $output .= '  /' . PHP_EOL
                . '  -';
        for($i=1; $i<=$max_length; $i++) {
            $output .= '-';
        }
        $output .= '-' . PHP_EOL;

        // ascii art image        
        $output .= '               \    |                                      
                \   |           @@@@&                              
                 \  |        @%@@@@@@@%@              @            
                  \ |       %%@&%@@@@@@@@            &&@           
                   \|       @@*,,,,,,,,&@           %&@            
                            @@,,****@@@/@(         %&@             
                            /**/(#,(#/,,%#        #&@              
                            #/*,*,,*,,,,//      &#&@               
                             @*,,*,*,,,*&      &%&@                
                              &,,*,,***/      %*(#                 
                              &#,,,*,*&/@    (##%&                 
                             #@.//*/(//#/&& /#%@*@%                
                          %@####%....@@,/&&*##,*&/&                
                      @@%%%&@&###%#.@@&@&@&##****#*#               
                    &%%%%%%%@&#####@,.&&/(%&@*,*,,*/&              
                   &%%%%%@%%%%@&####%/.@@,@&&*,,,,,@@              
                  @%%%%%%%%@%%%%@%####@@&@##@(,,**%&&@             
                  &&%%%%%%&&&%%%%&@####@&&@%#.%//&&@&&#            
                   &%%%%%%%%%&@%%%%&@##&@%%%%@/..(&@&&@            
                   @%%%%%%%%%%&&@%%%%@&%%%%%%%%@(@@@&&&&           
                    &%%%%%%%%%%%&&&&@@%%%%%%%%%%&@&@&&&&&          
                     &&%%%%%%%&&%@&%%%%%%%%%%@@@&&&@&&&&&&         
                      @&&%%%%%%&%%%%%%%%%&%@@@&&&&&@&&&&&&@        
                       &@&%%%%%%%%%%%%%%&&&@@@&&&&@@%%%%&&&@       
                       @&&@&&%%%%%%%%%&&&@&&@@&@&%%%%%%%%&&@       
                        @&&&@&&%%%%%&&@@@@&&&&%%%%%%%%%&&&&@       
                         &&&&&#@&&&@%@.@%%%%%%%%%%%%%&&&&&@        
                         @%&/%%*,**,*/..@%%%%%%%%%%%&&&@           
                         ((&#/@,*/,,*/@*&@%%%%&&@&@@               
                          &(@/@//(@&&&@@@@&@&&@&&&&@               
                          &&,%&%%@%%%%%%&&&&@@&&&&&&               ' . PHP_EOL;       
        
        fwrite(STDOUT, $output);
        exit(0);
    }

    
}
