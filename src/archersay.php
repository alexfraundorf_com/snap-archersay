#!/usr/bin/php
<?php
/*
 * Command script for a fun cowsay style command line program of Archer's famous 
 *  quotes.
 * 
 * WARNING: THIS CONTAINS GRAPHIC LANGUAGE AND IS NOT SAFE FOR WORK OR YOUNG CHILDREN!
 * 
 * @author Alex Fraundorf - AlexFraundorf.com - SnapProgramming.com
 * @copyright (c) 2019, Alex Fraundorf and/or Snap Programming and Development LLC
 * 
 * @package Snap\BobSay
 * @version 0.1.0 02/28/2019
 * @since 0.1.0 02/11/2019
 * @license MIT https://opensource.org/licenses/MIT
 */
namespace Snap\ArcherSay;

require_once(__DIR__ . '/vendor/autoload.php');

$Archer = new Archer();

$Archer
        ->setPathToQuotes(__DIR__ . '/vendor/snap/archersay/src/archerisms.txt')
        ->run($argv)
        ;
